This repo code contains optimizations for Z-Stack 3.0.1, they are provided in such a way that you can simply drag and drop the contents of the repo into your Z-Stack root directory and replace the corresponding files. Each optimization is on its own branch, checkout a branch by doing:

git checkout <branch name>

Current branches:

feature/GP-layer-optimizations
  Green Power proxy layer optimizations, Flash size reduction